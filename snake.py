import turtle
import random
import simpleaudio

SCREEN_WIDTH = 800
SCREEN_LENGTH = 600

isPlaying = True
isDeadPlayer1 = False
isDeadPlayer2 = False
update_delay = 250
last_points = 0

AUDIO_BACKGROUND = simpleaudio.WaveObject.from_wave_file("background.wav")
AUDIO_EAT = simpleaudio.WaveObject.from_wave_file("eat.wav")
AUDIO_DIE = simpleaudio.WaveObject.from_wave_file("die.wav")

turtle.setup(SCREEN_WIDTH, SCREEN_LENGTH)
screen = turtle.Screen()
screen.bgcolor("black")

DISTANCE_SNAKE = 25
ANGLE_0 = 0
ANGLE_90 = 90
ANGLE_180 = 2 * ANGLE_90
ANGLE_270 = 3 * ANGLE_90

HEAD_PLAYER_1 = turtle.Turtle()
HEAD_PLAYER_1.penup()
HEAD_PLAYER_1.speed(0)
HEAD_PLAYER_1.shape("turtle")
HEAD_PLAYER_1.color("white")
HEAD_PLAYER_1.goto(0, DISTANCE_SNAKE)

HEAD_PLAYER_2 = turtle.Turtle()
HEAD_PLAYER_2.penup()
HEAD_PLAYER_2.speed(0)
HEAD_PLAYER_2.shape("turtle")
HEAD_PLAYER_2.color("grey")
HEAD_PLAYER_2.goto(0, -DISTANCE_SNAKE)

DIRECTION_RIGHT = "Right"
DIRECTION_LEFT = "Left"
DIRECTION_UP = "Up"
DIRECTION_DOWN = "Down"

directionPlayer1 = DIRECTION_RIGHT
directionPlayer2 = DIRECTION_RIGHT

snakePlayer1 = []
snakePlayer2 = []
stampsPlayer1 = []
stampsPlayer2 = []
increasePlayer1 = False
increasePlayer2 = False

FOOD_X_BOUNDARY = 15
FOOD_Y_BOUNDARY = 11

SPRITE_FOOD_APPLE = turtle.Turtle()
SPRITE_FOOD_APPLE.penup()
SPRITE_FOOD_APPLE.speed(0)
SPRITE_FOOD_APPLE.shape("square")
SPRITE_FOOD_APPLE.color("red")
SPRITE_FOOD_APPLE.goto(random.randint(-FOOD_X_BOUNDARY, FOOD_X_BOUNDARY) * DISTANCE_SNAKE,
                       random.randint(-FOOD_Y_BOUNDARY, FOOD_Y_BOUNDARY) * DISTANCE_SNAKE)

SCORE_PLAYER_1 = turtle.Turtle()
SCORE_PLAYER_1.hideturtle()
SCORE_PLAYER_1.penup()
SCORE_PLAYER_1.speed(0)
SCORE_PLAYER_1.color("white")
SCORE_PLAYER_1.goto(-395, 275)
SCORE_PLAYER_1.write("Player 1: " + str(len(stampsPlayer1)), False, "left", ("Arial", 24, "bold"))

SCORE_PLAYER_2 = turtle.Turtle()
SCORE_PLAYER_2.hideturtle()
SCORE_PLAYER_2.penup()
SCORE_PLAYER_2.speed(0)
SCORE_PLAYER_2.color("white")
SCORE_PLAYER_2.goto(-395, 250)
SCORE_PLAYER_2.write("Player 2: " + str(len(stampsPlayer2)), False, "left", ("Arial", 24, "bold"))


def go_up_player1():
    global directionPlayer1

    if directionPlayer1 == DIRECTION_RIGHT:
        HEAD_PLAYER_1.left(ANGLE_90)
    elif directionPlayer1 == DIRECTION_LEFT:
        HEAD_PLAYER_1.right(ANGLE_90)

    if directionPlayer1 != DIRECTION_DOWN:
        directionPlayer1 = DIRECTION_UP


def go_up_player2():
    global directionPlayer2

    if directionPlayer2 == DIRECTION_RIGHT:
        HEAD_PLAYER_2.left(ANGLE_90)
    elif directionPlayer2 == DIRECTION_LEFT:
        HEAD_PLAYER_2.right(ANGLE_90)

    if directionPlayer2 != DIRECTION_DOWN:
        directionPlayer2 = DIRECTION_UP


def go_down_player1():
    global directionPlayer1

    if directionPlayer1 == DIRECTION_RIGHT:
        HEAD_PLAYER_1.right(ANGLE_90)
    elif directionPlayer1 == DIRECTION_LEFT:
        HEAD_PLAYER_1.left(ANGLE_90)

    if directionPlayer1 != DIRECTION_UP:
        directionPlayer1 = DIRECTION_DOWN


def go_down_player2():
    global directionPlayer2

    if directionPlayer2 == DIRECTION_RIGHT:
        HEAD_PLAYER_2.right(ANGLE_90)
    elif directionPlayer2 == DIRECTION_LEFT:
        HEAD_PLAYER_2.left(ANGLE_90)

    if directionPlayer2 != DIRECTION_UP:
        directionPlayer2 = DIRECTION_DOWN


def go_right_player1():
    global directionPlayer1

    if directionPlayer1 == DIRECTION_UP:
        HEAD_PLAYER_1.right(ANGLE_90)
    elif directionPlayer1 == DIRECTION_DOWN:
        HEAD_PLAYER_1.left(ANGLE_90)

    if directionPlayer1 != DIRECTION_LEFT:
        directionPlayer1 = DIRECTION_RIGHT


def go_right_player2():
    global directionPlayer2

    if directionPlayer2 == DIRECTION_UP:
        HEAD_PLAYER_2.right(ANGLE_90)
    elif directionPlayer2 == DIRECTION_DOWN:
        HEAD_PLAYER_2.left(ANGLE_90)

    if directionPlayer2 != DIRECTION_LEFT:
        directionPlayer2 = DIRECTION_RIGHT


def go_left_player1():
    global directionPlayer1

    if directionPlayer1 == DIRECTION_UP:
        HEAD_PLAYER_1.left(ANGLE_90)
    elif directionPlayer1 == DIRECTION_DOWN:
        HEAD_PLAYER_1.right(ANGLE_90)

    if directionPlayer1 != DIRECTION_RIGHT:
        directionPlayer1 = DIRECTION_LEFT


def go_left_player2():
    global directionPlayer2

    if directionPlayer2 == DIRECTION_UP:
        HEAD_PLAYER_2.left(ANGLE_90)
    elif directionPlayer2 == DIRECTION_DOWN:
        HEAD_PLAYER_2.right(ANGLE_90)

    if directionPlayer2 != DIRECTION_RIGHT:
        directionPlayer2 = DIRECTION_LEFT


def pause_game():
    global isPlaying
    global isDeadPlayer1
    global isDeadPlayer2

    if isPlaying:
        isPlaying = False
        simpleaudio.stop_all()
    elif not isDeadPlayer1 and not isDeadPlayer2:
        isPlaying = True
        AUDIO_BACKGROUND.play()
        update()


screen.onkey(go_up_player1, DIRECTION_UP)
screen.onkey(go_up_player2, "w")
screen.onkey(go_down_player1, DIRECTION_DOWN)
screen.onkey(go_down_player2, "s")
screen.onkey(go_right_player1, DIRECTION_RIGHT)
screen.onkey(go_right_player2, "d")
screen.onkey(go_left_player1, DIRECTION_LEFT)
screen.onkey(go_left_player2, "a")
screen.onkey(pause_game, "p")
screen.listen()


def eat():
    if HEAD_PLAYER_1.distance(SPRITE_FOOD_APPLE) < DISTANCE_SNAKE - 1:
        SPRITE_FOOD_APPLE.goto(random.randint(-FOOD_X_BOUNDARY, FOOD_X_BOUNDARY) * DISTANCE_SNAKE,
                               random.randint(-FOOD_Y_BOUNDARY, FOOD_Y_BOUNDARY) * DISTANCE_SNAKE)

        increase_snake_player1()

        AUDIO_EAT.play()
    elif HEAD_PLAYER_2.distance(SPRITE_FOOD_APPLE) < DISTANCE_SNAKE - 1:
        SPRITE_FOOD_APPLE.goto(random.randint(-FOOD_X_BOUNDARY, FOOD_X_BOUNDARY) * DISTANCE_SNAKE,
                               random.randint(-FOOD_Y_BOUNDARY, FOOD_Y_BOUNDARY) * DISTANCE_SNAKE)

        increase_snake_player2()

        AUDIO_EAT.play()


def increase_snake_player1():
    global increasePlayer1

    increasePlayer1 = True


def increase_snake_player2():
    global increasePlayer2

    increasePlayer2 = True


def move_player1():
    global increasePlayer1
    global stampsPlayer1
    global snakePlayer1

    stampsPlayer1 = [HEAD_PLAYER_1.stamp()] + stampsPlayer1
    snakePlayer1 = [HEAD_PLAYER_1.position()] + snakePlayer1

    if not increasePlayer1:
        HEAD_PLAYER_1.clearstamp(stampsPlayer1[len(stampsPlayer1) - 1])
        stampsPlayer1 = stampsPlayer1[0:len(stampsPlayer1) - 1]
        snakePlayer1 = snakePlayer1[0:len(snakePlayer1) - 1]
    else:
        SCORE_PLAYER_1.clear()
        SCORE_PLAYER_1.write("Player 1: " + str(len(stampsPlayer1)), False, "left", ("Arial", 24, "bold"))
        increasePlayer1 = False

    HEAD_PLAYER_1.forward(DISTANCE_SNAKE)


def move_player2():
    global increasePlayer2
    global stampsPlayer2
    global snakePlayer2

    stampsPlayer2 = [HEAD_PLAYER_2.stamp()] + stampsPlayer2
    snakePlayer2 = [HEAD_PLAYER_2.position()] + snakePlayer2

    if not increasePlayer2:
        HEAD_PLAYER_2.clearstamp(stampsPlayer2[len(stampsPlayer2) - 1])
        stampsPlayer2 = stampsPlayer2[0:len(stampsPlayer2) - 1]
        snakePlayer2 = snakePlayer2[0:len(snakePlayer2) - 1]
    else:
        SCORE_PLAYER_2.clear()
        SCORE_PLAYER_2.write("Player 2: " + str(len(stampsPlayer2)), False, "left", ("Arial", 24, "bold"))
        increasePlayer2 = False

    HEAD_PLAYER_2.forward(DISTANCE_SNAKE)


def head_is_on_tail_player1():
    tmp1 = map(lambda x: HEAD_PLAYER_1.distance(x), snakePlayer1 + [HEAD_PLAYER_2] + snakePlayer2)
    tmp1 = filter(lambda x: x < DISTANCE_SNAKE - 1, tmp1)
    tmp1 = list(tmp1)

    return len(tmp1) > 0


def head_is_on_tail_player2():
    tmp1 = map(lambda x: HEAD_PLAYER_2.distance(x), snakePlayer2 + [HEAD_PLAYER_1] + snakePlayer1)
    tmp1 = filter(lambda x: x < DISTANCE_SNAKE - 1, tmp1)
    tmp1 = list(tmp1)

    return len(tmp1) > 0


def die():
    global isDeadPlayer1
    global isDeadPlayer2

    x_boundary = FOOD_X_BOUNDARY * DISTANCE_SNAKE
    y_boundary = FOOD_Y_BOUNDARY * DISTANCE_SNAKE

    position_player1 = HEAD_PLAYER_1.position()
    x_coord_player1 = position_player1[0]
    y_coord_player1 = position_player1[1]

    position_player2 = HEAD_PLAYER_2.position()
    x_coord_player2 = position_player2[0]
    y_coord_player2 = position_player2[1]

    if abs(x_coord_player1) > x_boundary + 1 or abs(y_coord_player1) > y_boundary + 1 or head_is_on_tail_player1():
        isDeadPlayer1 = True
    if abs(x_coord_player2) > x_boundary + 1 or abs(y_coord_player2) > y_boundary + 1 or head_is_on_tail_player2():
        isDeadPlayer2 = True

    if isDeadPlayer1 or isDeadPlayer2:
        simpleaudio.stop_all()
        AUDIO_DIE.play()

        if len(stampsPlayer1) - len(stampsPlayer2) >= 4:
            display_winner("Player 1")
        elif len(stampsPlayer1) - len(stampsPlayer2) <= -4:
            display_winner("Player 2")
        else:
            if isDeadPlayer1 and isDeadPlayer2:
                display_winner("No one")
            elif isDeadPlayer1:
                display_winner("Player 2")
            else:
                display_winner("Player 1")


def display_winner(winner_string):
    winner = turtle.Turtle()
    winner.hideturtle()
    winner.penup()
    winner.speed(0)
    winner.color("gold")
    winner.goto(0, 0)
    winner.write(winner_string + " won the game!", False, "center", ("Arial", 50, "bold"))


def update():
    global update_delay
    global last_points

    move_player1()
    move_player2()
    die()
    eat()

    if isPlaying and (not isDeadPlayer1 and not isDeadPlayer2):
        points = len(stampsPlayer1) + len(stampsPlayer2)
        if points % 3 == 0 and points != last_points and update_delay > 25:
            update_delay -= 25
            last_points = points

        screen.ontimer(update, update_delay)


AUDIO_BACKGROUND.play()
update()

screen.mainloop()
